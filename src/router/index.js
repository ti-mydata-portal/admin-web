import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']    will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if true ,the page will no be cached(default is false)
  }
**/
export const constantRouterMap = [

  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },

  {
    path: '',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'dashboard', icon: 'icom-dashboard', noCache: true }
      }
    ]
  }
]

export default new Router({
  // mode: 'history',
  // hash: false,
  routes: constantRouterMap
})

export const asyncRouterMap = [
  // API
  {
    path: '/api',
    component: Layout,
    redirect: '/api',
    meta: { title: 'api', icon: 'icon-api' },
    alwaysShow: true,
    children: [
      {
        path: '/apiManage',
        component: () => import('@/views/api/apiManage/index'),
        name: 'ApiManage',
        meta: { title: 'apiManage', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      {
        path: '/apiManage/apiEdit',
        component: () => import('@/views/api/apiManage/apiEdit'),
        name: 'ApiEdit',
        meta: { title: 'apiEdit', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      },
      {
        path: '/apiManage/apiVersionUp',
        component: () => import('@/views/api/apiManage/apiVersionUp'),
        name: 'ApiVersionUp',
        meta: { title: 'apiVersionUp', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      },
      {
        path: '/apiManage/apiRegisterIndividual',
        component: () => import('@/views/api/apiManage/registerIndividual'),
        name: 'RegisterIndividual',
        meta: { title: 'registerIndividual', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/apiManage/apiRegisterUnify',
        component: () => import('@/views/api/apiManage/registerUnify'),
        name: 'RegisterUnify',
        meta: { title: 'registerUnify', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/apiManage/apiPolicy',
        component: () => import('@/views/api/apiManage/apiPolicy'),
        name: 'ApiPolicy',
        meta: { title: 'apiPolicy', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/apiEchoManage',
        component: () => import('@/views/api/apiEchoManage/index'),
        name: 'ApiEchoManage',
        meta: { title: 'apiEchoManage', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] }
      },
      {
        path: '/apiEchoManage/create',
        component: () => import('@/views/api/apiEchoManage/createEcho'),
        name: 'ApiEchoCreate',
        meta: { title: 'apiEchoCreate', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/apiEchoManage/detail',
        component: () => import('@/views/api/apiEchoManage/detailEcho'),
        name: 'ApiEchoDetail',
        meta: { title: 'apiEchoDetail', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/apiEchoManage/edit',
        component: () => import('@/views/api/apiEchoManage/editEcho'),
        name: 'ApiEchoEdit',
        meta: { title: 'apiEchoEdit', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/guideMain',
        component: () => import('@/views/guide/guideMain/index'),
        name: 'GuidePortal',
        meta: { title: 'developGuide', noCache: true }
      },
      {
        path: '/guideMain/guideDetail',
        component: () => import('@/views/guide/guideMain/guideDetail'),
        name: 'GuidePortalDetail',
        meta: { title: 'guidePortalDetail', noCache: true },
        hidden: true
      }
    ]
  },
  // APP(TO-BE)
  {
    path: '/tApp',
    component: Layout,
    redirect: '/tApp',
    children: [
      {
        path: '/tAppManage',
        component: () => import('@/views/app/tAppManage/index'),
        name: 'tAppManage',
        meta: { title: 'Service(APP)', icon: 'icon-service', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      {
        path: '/tAppManage/appCreate',
        component: () => import('@/views/app/tAppManage/appCreate'),
        name: 'tAppCreate',
        meta: { title: 'appCreate', icon: 'app', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/tAppManage/appEdit',
        component: () => import('@/views/app/tAppManage/appEdit'),
        name: 'tAppEdit',
        meta: { title: 'appDetail', icon: 'app', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      },
      {
        path: '/tAppManage/appDetail',
        component: () => import('@/views/app/tAppManage/appDetail'),
        name: 'tAppDetail',
        meta: { title: 'appDetail', icon: 'app', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      },
      {
        path: '/tAppManage/appPolicy',
        component: () => import('@/views/app/tAppManage/appPolicy'),
        name: 'tAppPolicy',
        meta: { title: 'appPolicy', icon: 'app', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      }
    ]
  },
  // 수신자관리(기관)
  {
    path: '/useOrg',
    component: Layout,
    redirect: '/useOrg',
    children: [
      {
        path: '/useOrgList',
        component: () => import('@/views/useOrg/index'),
        name: 'useOrgList',
        meta: { title: '수신자관리(기관)', icon: 'icon-recipient', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      {
        path: '/useOrgList/useOrgCreate',
        component: () => import('@/views/useOrg/useOrgCreate'),
        name: 'useOrgCreate',
        meta: { title: '수신자관리(기관) 등록', icon: 'app', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/useOrgList/useOrgEdit',
        component: () => import('@/views/useOrg/useOrgEdit'),
        name: 'useOrgEdit',
        meta: { title: '수신자관리(기관) 수정', icon: 'app', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      }
    ]
  },
  // 유량제어
  {
    path: '/servicePolicy',
    component: Layout,
    redirect: '/servicePolicy',
    meta: {
      title: '정책', icon: 'icon_policy', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN']
    },
    children: [
      {
        path: '/servicePolicyList',
        component: () => import('@/views/servicePolicy/index'),
        name: 'servicePolicyList',
        meta: { title: '정책', icon: 'icon_policy', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] }
      },
      {
        path: '/servicePolicyList/servicePolicyCreate',
        component: () => import('@/views/servicePolicy/servicePolicyCreate'),
        name: 'servicePolicyCreate',
        meta: { title: '유량제어 정책 등록', icon: 'app', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      },
      {
        path: '/servicePolicyList/servicePolicyEdit',
        component: () => import('@/views/servicePolicy/servicePolicyEdit'),
        name: 'servicePolicyEdit',
        meta: { title: '유량제어 정책 수정', icon: 'app', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
        hidden: true
      }
    ]
  },
  // 통계
  {
    path: '/statistics',
    component: Layout,
    redirect: '/statistics',
    alwaysShow: true,
    meta: {
      title: '통계', icon: 'icon-graph', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN']
    },
    children: [
      // to-be 일자별 API 사용통계
      {
        path: '/statisticsApi',
        component: () => import('@/views/statistics/api/index'),
        name: 'statisticsApiDay',
        meta: { title: '일자별 API 사용통계', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      // to-be 일자별 사업자 사용통계
      {
        path: '/statisticsBiz',
        component: () => import('@/views/statistics/biz/index'),
        name: 'statisticsBizDay',
        meta: { title: '일자별 사업자 사용통계', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      // 마이데이터 이용통계
      {
        path: '/statisticsMydata',
        component: () => import('@/views/statistics/mydata/index'),
        name: 'statisticsMydata',
        meta: { title: '마이데이터 사용통계', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] }
      }
    ]
  },
  // 로그
  {
    path: '/log',
    component: Layout,
    redirect: '/log',
    alwaysShow: true,
    meta: {
      title: 'log', icon: 'icon-log', roles: ['ROLE_SYS_ADMIN']
    },
    children: [
      {
        path: '/logApiDeal',
        component: () => import('@/views/log/apiDeal/index'),
        name: 'ApiDeal',
        meta: { title: 'apiDeal', roles: ['ROLE_SYS_ADMIN'] }
      },
      {
        path: '/logPortalUse',
        component: () => import('@/views/log/portalUse/index'),
        name: 'PortalUse',
        meta: { title: 'portalUse', roles: ['ROLE_SYS_ADMIN'] }
      },
      {
        path: '/batchHistory',
        component: () => import('@/views/admin/batchHistory/index'),
        name: 'BatchHistory',
        meta: { title: 'batchHistory', roles: ['ROLE_SYS_ADMIN'] }
      }
    ]
  },
  {
    path: '/account',
    component: Layout,
    redirect: '/account',
    meta: {
      title: '계정관리', icon: 'icon-setting', roles: ['ROLE_SYS_ADMIN']
    },
    children: [
      {
        path: '/account',
        component: () => import('@/views/account/index'),
        name: 'Account',
        meta: { title: '계정관리', icon: 'icon-accounts', roles: ['ROLE_SYS_ADMIN'] }
      },
      {
        path: '/account/accountDetail',
        component: () => import('@/views/account/accountDetail'),
        name: 'AccountDetail',
        meta: { title: '계정수정', roles: ['ROLE_SYS_ADMIN'] },
        hidden: true
      },
      {
        path: '/account/accountCreate',
        component: () => import('@/views/account/accountCreate'),
        name: 'AccountCreate',
        meta: { title: '계정수정', roles: ['ROLE_SYS_ADMIN'] },
        hidden: true
      }
    ]
  },
  // 고객관리
  {
    path: '/setting',
    component: Layout,
    redirect: '/setting',
    hidden: true,
    meta: { title: 'setting', icon: 'setting' },
    children: [
      {
        path: '/my_info',
        component: () => import('@/views/setting/my_info/index'),
        name: 'MyInfo',
        meta: { title: 'myInfo', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      }
    ]
  },
  // 수수료정산
  {
    path: '/settlement',
    component: Layout,
    redirect: '/settlement',
    hidden: true,
    meta: {
      title: 'feeSettlement', icon: 'icon-search', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN']
    },
    children: [
      {
        path: '/paymentHistory/payment',
        component: () => import('@/views/settlement/paymentHistory/payment'),
        name: 'payment',
        meta: { title: 'payment', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      {
        path: '/paymentHistory/settlement',
        component: () => import('@/views/settlement/paymentHistory/settlement'),
        name: 'settlement',
        meta: { title: 'settlement', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      {
        path: '/billingManagement',
        component: () => import('@/views/settlement/billingManagement/index'),
        name: 'BillingIndex',
        meta: { title: 'billingManagement', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      {
        path: '/billingManagement/register',
        component: () => import('@/views/settlement/billingManagement/register'),
        name: 'BillingReg',
        meta: { title: 'BillingReg', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      },
      {
        path: '/billingManagement/update',
        component: () => import('@/views/settlement/billingManagement/update'),
        name: 'BillingUpd',
        meta: { title: 'BillingUpdate', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      }
    ]
  },
  // 정책설정
  {
    path: '/policy',
    component: Layout,
    redirect: '/policy',
    hidden: true,
    meta: {
      title: 'policy', icon: 'component', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN']
    },
    children: [
      {
        path: '/getTokenPolicy',
        component: () => import('@/views/policy/getTokenPolicy'),
        name: 'GetTokenPolicy',
        meta: { title: 'getTokenPolicy' }
      },
      {
        path: '/getTokenPolicyDetail',
        component: () => import('@/views/policy/getTokenPolicyDetail'),
        name: 'GetTokenPolicyDetail',
        meta: { title: 'getTokenPolicyDetail' },
        hidden: true
      }
    ]
  },
  // 보안감사
  {
    path: '/security',
    component: Layout,
    redirect: '/security',
    hidden: true,
    meta: {
      title: 'security', icon: 'excel', roles: ['ROLE_SYS_ADMIN']
    },
    children: [
      {
        path: '/accounts',
        component: () => import('@/views/security/accounts/index'),
        name: 'Accounts',
        meta: { title: 'account' }
      },
      {
        path: '/createAccount',
        component: () => import('@/views/security/accounts/create'),
        name: 'CreateAccount',
        meta: { title: 'hfnUsereCreate' },
        hidden: true
      },
      {
        path: '/detailAccount',
        component: () => import('@/views/security/accounts/detail'),
        name: 'DetailAccount',
        meta: { title: 'hfnUserDetail' },
        hidden: true
      },
      // {
      //   path: '/accountModify',
      //   component: () => import('@/views/security/accounts/modify'),
      //   name: 'AccountModify',
      //   meta: { title: 'hfnUserEdit', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] },
      //   hidden: true
      // },
      {
        path: '/connectionHistory',
        component: () => import('@/views/security/connectionHistory'),
        name: 'ConnectionHistory',
        meta: { title: 'connectionHistory' }
      },
      {
        path: '/personalInfo',
        component: () => import('@/views/security/personalInfo'),
        name: 'PersonalInfo',
        meta: { title: 'personalInfo' }
      },
      {
        path: '/activityHistory',
        component: () => import('@/views/security/activityHistory'),
        name: 'ActivityHistory',
        meta: { title: 'activityHistory' }
      }
    ]
  },
  // 포털관리
  {
    path: '/admin',
    component: Layout,
    redirect: '/admin',
    alwaysShow: true,
    meta: {
      title: 'admin', icon: 'icon-setting', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN', 'ROLE_ORG_ADMIN']
    },
    children: [
      {
        path: '/apiServiceSetting',
        component: () => import('@/views/setting/service/index'),
        name: 'ApiServiceSetting',
        meta: { title: 'apiServiceSetting', roles: ['ROLE_SYS_ADMIN'] }
      },
      {
        path: '/innerTokenSetting',
        component: () => import('@/views/setting/service/innerTokenSetting'),
        name: 'InnerTokenSetting',
        meta: { title: 'innerTokenSetting', roles: ['ROLE_SYS_ADMIN'] }
      },
      {
        path: '/redis',
        component: () => import('@/views/policy/redis'),
        name: 'GetRedis',
        meta: { title: 'getRedis', roles: ['ROLE_SYS_ADMIN', 'ROLE_HFN_ADMIN'] }
      },
      {
        path: '/code',
        component: () => import('@/views/admin/code/codeList'),
        name: 'CodeList',
        meta: { title: 'codeList', roles: ['ROLE_SYS_ADMIN'] }
      },
      {
        path: '/manageTrx',
        component: () => import('@/views/admin/manageTrx/index'),
        name: 'ManageTrx',
        meta: { title: 'manageTrx', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN'] }
      },
      {
        path: '/regTrx',
        component: () => import('@/views/admin/manageTrx/regTrx'),
        name: 'RegTrx',
        meta: { title: 'regTrx', roles: ['ROLE_SYS_ADMIN'] },
        hidden: true
      },
      {
        path: '/detailTrx',
        component: () => import('@/views/admin/manageTrx/detailTrx'),
        name: 'DetailTrx',
        meta: { title: 'detailTrx', roles: ['ROLE_SYS_ADMIN', 'ROLE_ORG_ADMIN'] },
        hidden: true
      }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]
