import Vue from 'vue'
import i18n from '@/lang'
import moment from 'moment'
import { isEmpty } from '@/utils/validate'

Vue.filter('svcNmFilter', function (code, txtParam, codeList) {
  const value = codeList.reduce((acc, cur) => {
    acc[cur.cdId] = cur.cdNm
    return acc
  }, {})
  return value[code]
})

Vue.filter('svcEtcFilter', function (code, txtParam, codeList) {
  const value = codeList.reduce((acc, cur) => {
    acc[cur.cdId] = cur.cdEtc
    return acc
  }, {})
  return value[code]
})

Vue.filter('dateFilter', function (data) {
  if (!isEmpty(data)) {
    data = moment(data).format("YYYY-MM-DD")
  }
  return data
})
Vue.filter('statsFilter', function (data) {
  if (!isEmpty(data)) {
    if (data === '00') {
      data = '00:00 ~ 02:59'
    } else if (data === '01') {
      data = '03:00 ~ 05:59'
    } else if (data === '02') {
      data = '06:00 ~ 08:59'
    } else if (data === '03') {
      data = '09:00 ~ 11:59'
    } else if (data === '04') {
      data = '12:00 ~ 14:59'
    } else if (data === '05') {
      data = '15:00 ~ 17:59'
    } else if (data === '06') {
      data = '18:00 ~ 20:59'
    } else if (data === '07') {
      data = '21:00 ~ 23:59'
    }
  }
  return data
})
Vue.filter('apiTypeFilter', function(data) {
  if (!isEmpty(data)) {
    if (data === 'AU01' || data === 'AU02')
      data = '개별인증'
    else if (data === 'CM01' || data === 'CM02')
      data = '공통'
    else if (data === 'BA01')
      data = '계좌 목록'  
    else if (data === 'BA02' || data === 'BA03' || data === 'BA04')
      data = '수신계좌 정보'  
    else if (data === 'BA11' || data === 'BA12' || data === 'BA13')
      data = '투자상품 정보'  
    else if (data === 'BA21' || data === 'BA22')
      data = '대출상품 정보'  
  }
  return data
})
Vue.filter('dateDotFilter', function (data) {
  if (!isEmpty(data)) {
    data = moment(data).format("YYYY.MM.DD")
  }
  return data
})

Vue.filter('dtFilter', function (data) {
  if (!isEmpty(data)) {
    data = moment(data).format("YYYY-MM-DD HH:mm:ss")
  }
  return data
})

Vue.filter('addDtFilter', function (data) {
  if (!isEmpty(data)){
    data = moment(data,'YYYY-MM-DD HH:mm:ss').format("YYYY-MM-DD HH:mm:ss")
  }
  return data
})

Vue.filter('hmFilter', function (data) {
  if (!isEmpty(data)) {
    data = moment(data, "hmm").format("HH:mm")
  }
  return data
})

Vue.filter('wonFilter', function (data) {
  if (!isEmpty(data)){
    data = parseFloat((data + '').replace(/[^\d\.-]/g, '')) + ''
    let l = data.split('.')[0].split('').reverse()
    let t = ''
    for (let i = 0; i < l.length; i++) {
      t += l[i] + ((i + 1) % 3 === 0 && (i + 1) !== l.length ? ',' : '')
    }
    data = t.split('').reverse().join('')
  }
  return data
})
// 사업자등록번호 하이픈(-) 추가 포맷
Vue.filter('brnFilter', function (data) {
  var formatData = '';
  data = data.replace('-','')
  if (!isEmpty(data)){
    if (data.length == 10) {
      formatData = data.replace(/(\d{3})(\d{2})(\d{5})/, '$1-$2-$3');
    } else {
      formatData = data
    }
  }
  return formatData
})
Vue.filter('roleCdFilter', function (data) {
  const nameValue = {
    10: i18n.t('user.portalManage'),
    20: i18n.t('user.api'),
    21: i18n.t('user.dev'),
    22: i18n.t('user.sales'),
    30: i18n.t('user.officeManage'),
    31: i18n.t('user.officeEmployee'),
    40: i18n.t('user.individual')
  }
  return nameValue[data]
})

Vue.filter('roleNmFilter', function (data) {
  const nameValue = {
    'ROLE_SYS_ADMIN': i18n.t('security.sysRole'),
    'ROLE_HFN_ADMIN': i18n.t('security.apiRole'),
    'ROLE_ORG_ADMIN': i18n.t('security.devRole'),
    'ROLE_HFN_SALES': i18n.t('security.salesRole'),
    'ROLE_PERSONAL': i18n.t('security.userRole')
  }
  return nameValue[data]
})

Vue.filter('siteNmFilter', function (data) {
  const nameValue = {
    'adminPortal': i18n.t('log.aportal'),
    'userPortal': i18n.t('log.uportal'),
    'backoffice': i18n.t('log.bo'),
    'pvBatch': i18n.t('log.batch')
  }
  return nameValue[data]
})
