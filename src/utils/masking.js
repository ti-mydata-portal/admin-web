import Vue from 'vue'
import { isEmpty } from '@/utils/validate'

Vue.filter('maskingId', function (data) {
  if (!isEmpty(data) && data != 'DEL') {
    // 정규식 ie 오류
    // return data.replace(/(?<=.{3})./gi, '*')
    //  /(?!^)(?<!^.{0,2})./g     (O)
    const head = new RegExp(`^.{3}`, 'g').exec(data);
    const tails = data.replace(head, '').replace(/./g, '*')
    return head + tails
  } else if (data == 'DEL') {
    return '(탈퇴이용자)'
  } else {
    return ''
  }
})

Vue.filter('maskingName', function (name) {
  var data = ''
  if (!isEmpty(name) && name != 'DEL') {
    for (let i = 0; i < name.length; i++) {
      if (i === 1) {  
        data += '*'
      } else {
        data += name.charAt(i)
      }
    }
  }
  return data
})

Vue.filter('maskingCompRegNo', function (num, type) {
  var data = ''
  if (null != num ) {
    if (num.length == 10) {
      if (type == 0) {
        data = num.replace(/(\d{3})(\d{2})(\d{5})/, '$1-$2-*****')
      } else {
        data = num.replace(/(\d{3})(\d{2})(\d{5})/, '$1-$2-$3')
      }
    }
  }
  return data
})

// 마스킹: 아이디
export function maskingId(string) {
  if (string) {
    const head = new RegExp(`^.{3}`, 'g').exec(string);
    const tails = string.replace(head, '').replace(/./g, '*')
    return head + tails
  } else {
    return ''
  }
}

// 마스킹: 이름
export function maskingName(string) {
  if (string) {
    let tempString = ''
    for (let i = 0; i < string.length; i++) {
      if (i === 1) {
        tempString += '*'
      } else {
        tempString += string.charAt(i)
      }
    }
    return tempString
  } else {
    return ''
  }
}

// 마스킹: 전화번호
export function maskingTel(string) {
  if (string) {
    if (string.length === 11) {
      return string.substr(0, 3) + '-' + string.substr(3, 2) + '**' + '-' + string.substr(7, 2) + '**'
    } else {
      return string.substr(0, 2) + '**' + '-' + string.substr(4, 2) + '**'
    } 
  } else {
    return ''
  }
}

// 마스킹: 이메일
export function maskingEmail(string) {
  if (string) {
    const len = string.split('@')[0].length - 3
    return string.replace(new RegExp('.(?=.{0,' + len + '}@)', 'g'), '*')
  } else {
    return ''
  }
}

// 마스킹: 계좌번호
export function maskingBankNo(string) {
  if (string) {
    let tempString = ''
    for (let i = 0; i < string.length; i++) {
      if (i > 3 && i < 9) {
        tempString += '*'
      } else {
        tempString += string.charAt(i)
      }
    }
    return tempString
  } else {
    return ''
  }
}

// 마스킹: 사업자번호 maskingCompRegNo('123',)
// export function maskingCompRegNo(num, type) {
//   var formatNum = ''
//   if (num.length === 10) {
//     if (type === 0) {
//       formatNum === num.replace(/(\d{3})(\d{2})(\d{5})/, '$1-$2-*****')
//     } else {
//       formatNum === num.replace(/(\d{3})(\d{2})(\d{5})/, '$1-$2-$3')
//     }
//   }
//   return formatNum
// }
