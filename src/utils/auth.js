import Cookies from 'js-cookie'

const TokenKey = 'Admin-Portal-Token'
const ModalKey = 'Welcome-Portal-Modal'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getWelcomeModal() {
  return Cookies.get(ModalKey)
}

export function setWelcomeModal(value) {
  return Cookies.set(ModalKey, value)
}

export function removeModal() {
  return Cookies.remove(ModalKey)
}
