export function comma(num) {
    var len, point, str
    num = num + ""
    point = num.length % 3
    len = num.length
  
    str = num.substring(0, point)
    while (point < len) {  
        if (str !==  "") str += ","
        str += num.substring(point, point + 3)
        point += 3
    }
    
    return str;
}
  
export function dateFormat(value, format) {
    var d = value;

    if (!value.valueOf()){
        d = new Date()
    }else{
        if(typeof value == "string"){
            d = new Date(value)
        }
    }

    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];

    var strLpad = function(strVal, len){
        return "0".substring(0,len - strVal.length) + strVal;
    }

    var numLpad = function(numVal, len){
        return strLpad(numVal.toString(),len);
    }

    return format.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return numLpad((d.getFullYear() % 1000),2);
            case "MM": return numLpad((d.getMonth() + 1),2);
            case "dd": return numLpad(d.getDate(),2);
            case "E": return weekName[d.getDay()];
            case "HH": return numLpad(d.getHours(),2);
            case "hh": return numLpad(((h = d.getHours() % 12) ? h : 12),2);
            case "mm": return numLpad(d.getMinutes(),2);
            case "ss": return numLpad(d.getSeconds(),2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
}