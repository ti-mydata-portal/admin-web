import restrict from './restrict'

const install = function(Vue) {
  Vue.directive('restrict', restrict)
}

if (window.Vue) {
  window.restrict = restrict
  Vue.use(install) // eslint-disable-line
}

restrict.install = install
export default restrict
