import { login, getInfo, logout } from '@/api/login'
import { getToken, setToken, removeToken, getWelcomeModal, setWelcomeModal, removeModal } from '@/utils/auth'
import { setCookie, delCookie } from '@/utils'

const user = {
  state: {
    token: getToken(),
    userKey: '',
    modal: getWelcomeModal(),
    username: '',
    avatar: '',
    roles: [],
    hfnCd: '',
    hfnId: '',
    codeList: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USERKEY: (state, userKey) => {
      state.userKey = userKey
    },
    SET_WELCOME_MODAL_NAME: (state, name) => {
      state.modal = name
    },
    // 사용자 계정명
    SET_USERNAME: (state, username) => {
      state.username = username
    },
    SET_HFNCD: (state, hfnCd) => {
      state.hfnCd = hfnCd
    },
    SET_HFNID: (state, hfnId) => {
      state.hfnId = hfnId
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    }
  },

  actions: {
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim()
      // 저장전， 기존 계정명 저장 내용 존제시 삭제 처리
      removeModal()
      delCookie('idSaveCheck')
      delCookie('certSaveCheck')

      if (userInfo.isSaveId) {
        commit('SET_USERNAME', username)
        setCookie('idSaveCheck', username)
      }

      return new Promise((resolve, reject) => {
        login(username, userInfo.password, 'adminPortal', userInfo.hfnCd, userInfo.hfnId).then(response => {
          if(response.accessToken == null || response.accessToken == ""){
            reject('getInfo: roles must be a non-null array !')
          }else{
            if (response.tmpPasswordYn === 'N') {
              setToken(response.accessToken)
              commit('SET_TOKEN', response.accessToken)
            } else {
              setToken(response.accessToken)
              commit('SET_TOKEN', response.accessToken)
              var modalName = 'modalA'
              if (response.portalTosYn === 'Y') {
                modalName = 'modalB'
              }
              commit('SET_WELCOME_MODAL_NAME', modalName)
              setWelcomeModal(modalName)
            }
            resolve(response)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 관리자 정보 조회
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo(state.token).then(response => {
          if (response.authorities && response.authorities.length > 0) {
            var roles = []
            response.authorities.forEach(function(value) {
              roles.push(value.authority)
            })
            commit('SET_ROLES', roles)
          } else {
            reject('getInfo: roles must be a non-null array !')
          }
          commit('SET_USERNAME', response.username)
          commit('SET_USERKEY', response.userKey)
          commit('SET_HFNCD', response.hfnCd)
          commit('SET_HFNID', response.userId)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // logout
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout().then(response => {
          resolve()
          commit('SET_TOKEN', '')
          commit('SET_WELCOME_MODAL_NAME', '')
          commit('SET_ROLES', [])
          commit('SET_USERNAME', '')
          commit('SET_USERKEY', '')
          commit('SET_HFNCD', '')
          commit('SET_HFNID', '')
          commit('SET_AVATAR', '')
          commit('SET_DEFAULT_ROUTES')
          removeToken()
          removeModal()
        }).catch(error => {
          reject(error)
        })
      })
    },

    FedLogOut({ commit }) {
      return new Promise(resolve => {
        logout().then(response => {
          resolve()
        })
        commit('SET_TOKEN', '')
        commit('SET_WELCOME_MODAL_NAME', '')
        commit('SET_ROLES', [])
        commit('SET_USERNAME', '')
        commit('SET_USERKEY', '')
        commit('SET_HFNCD', '')
        commit('SET_HFNID', '')
        commit('SET_AVATAR', '')
        commit('SET_DEFAULT_ROUTES')
        removeToken()
        removeModal()
      })
    },
    SetToken({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', getToken())
        resolve()
      })
    },
    SetTokenNull({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
