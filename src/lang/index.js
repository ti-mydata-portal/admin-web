import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'
import elementKoLocale from 'element-ui/lib/locale/lang/ko'// element-ui lang
import elementEnLocale from 'element-ui/lib/locale/lang/en' // element-ui lang
import koLocale from './ko'
import enLocale from './en'

Vue.use(VueI18n)

const messages = {
  ko: {
    ...koLocale,
    ...elementKoLocale
  },
  en: {
    ...enLocale,
    ...elementEnLocale
  }
}

const i18n = new VueI18n({
  // set locale
  locale: Cookies.get('language') || 'ko',
  // set locale messages
  messages
})

export default i18n
