import request from './request/portal'

export function appList(data) {
  const url = '/api/appListSelected'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateAppStat(data) {
  const url = '/api/appUpdateStatCd'
  const params = {
    appKey: data.appKey,
    appClientId: data.appClientId,
    appStatCd: data.appStatCd
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function getApiList(data) {
  const url = '/api/apis'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
export function appRegister(data) {
  const url = '/api/appReg'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function appDelete(data) {
  const url = '/api/appDel'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchAppInfo(params) {
  const url = '/api/appDetail'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function updateAppInfo(data) {
  const url = '/api/appUpdate'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function appServiceExtend(data) {
  const url = '/api/appExtend'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function reissue(data) {
  const url = '/api/appSecretReisu'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function hfnCompanyList(data) {
  const url = '/api/hfnCompanyAll'
  const params = {
    data: data
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function keyDownload(data) {
  const url = '/api/appCidSecretDownload'
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'responseType': 'arraybuffer'
    }
  }
  return request.post(url, data, config)
}

export function fetchAppUseDefresYn(appKey) {
  const url = '/api/appDefresUseYn'
  const params = {
    appKey: appKey
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function appUseDefresChange(data) {
  const url = '/api/appDefresChange'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function selectAppUserList(data) {
  const url = '/api/selectAppUserList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서비스(앱) 조회
export function selectServiceAppList(data) {
  const url = '/api/selectServiceAppList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서비스(앱) 상태변경
export function serviceAppStatusUpd(data) {
  const url = '/api/serviceAppStatusUpd'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서비스(앱) 기관검색
export function searchUseorg(data) {
  const url = '/api/searchUseorg'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서비스(앱) 등록
export function serviceAppReg(data) {
  const url = '/api/serviceAppReg'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서비스(앱) 상세조회
export function serviceAppDetail(data) {
  const url = '/api/serviceAppDetail'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서비스(앱) IP정보 조회
export function selectServiceAppIps(data) {
  const url = '/api/selectServiceAppIps'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서비스(앱) 정보 수정
export function serviceAppUpd(data) {
  const url = '/api/serviceAppUpd'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서비스(앱) 통계 조회 
export function selectServiceAppStatsList(data) {
  const url = '/api/selectServiceAppStatsList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}