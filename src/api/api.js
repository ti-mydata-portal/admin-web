import request from './request/portal'

export function apiDelete(data) {
  const url = '/api/apiDelete'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiDetail(apiId) {
  const url = '/api/api'
  const params = {
    apiId: apiId
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function apiRegister(data) {
  const url = '/api/apiRegist'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiList(data) {
  const url = '/api/apis'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiListNoPaging(data) {
  const url = '/api/apiListNoPaging'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateApiStatus(data) {
  const url = '/api/apiStatCdChange'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function editApi(data) {
  const url = '/api/apiUpdate'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiSwaggerList(data) {
  const url = '/api/swaggerList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiSwaggerInfo(data) {
  const url = '/api/swaggerInfo'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function refreshToken(data) {
  const url = '/auth/tokenTimeContinue'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiEchoList(data) {
  const url = '/api/selectApiEcho'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApilistForEcho(data) {
  const url = '/api/selectApisForEcho'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiResponseList(data) {
  const url = '/api/getApiResponseSetting'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiRequestList(data) {
  const url = '/api/getApiRequestSetting'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function regApiEcho(data) {
  const url = '/api/regApiEcho'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateApiEcho(data) {
  const url = '/api/updateApiEcho'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function detailApiEcho(data) {
  const url = '/api/detailApiEcho'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function apiEchos(data) {
  const url = '/api/apiEchos'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function deleteApiEcho(data) {
  const url = '/api/deleteApiEcho'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function regApiSearchKey(data) {
  const url = '/api/regApiSearchKey'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateApiSearchKey(data) {
  const url = '/api/updateApiSearchKey'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function getApiSearchKey(data) {
  const url = '/api/getApiSearchKey'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function getAppList(data) {
  const url = '/settlement/getAppList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
