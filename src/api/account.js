import request from './request/portal'
// 수신자관리 조회 appList
export function useOrgList(data) {
    const url = '/api/accountList'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }