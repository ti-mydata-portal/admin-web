import request from './request/portal'

export function selectCommCodeList() {
  const url = '/api/selectPrdCommList'
  const params = {}
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function selectProductList(data) {
  const url = '/api/selectProductList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}


export function updatePrdOrder(data) {
  const url = '/api/updatePrdOrder'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
