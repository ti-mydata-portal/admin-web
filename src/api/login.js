import request from './request/portal'

// TODO NICE XX
// export function hfnGroupList() {
//   const url = '/auth/hfnGroupCode'
//   const params = {}
//   const config = {
//     headers: {
//       'Content-Type': 'application/json'
//     }
//   }
//   return request.post(url, params, config)
// }

export function login(username, password, siteCd, hfnCd, hfnId) {
  const url = '/auth/hfnLogin'
  const param = {
    username: username,
    password: password,
    siteCd: siteCd,
    hfnCd: hfnCd,
    hfnId: hfnId
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, param, config)
}

export function getInfo(token) {
  const url = '/user/select'
  const param = {
    token: token
  }

  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, param, config)
}

export function logout() {
  const url = '/auth/hfnLogout'
  const param = {
  }

  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  return request.post(url, param, config)
}

export function signUp(data) {
  const url = '/auth/signup'
  const param = {
    name: data.name,
    username: data.username,
    password: data.password,
    userTel: data.userTel,
    brn: data.brn,
    useorgNm: data.useorgNm,
    provideUseorgCd: data.provideUseorgCd,
    useorgCtnt: data.useorgCtnt
  }

  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  return request.post(url, param, config)
}

export function signUpCheckOfficeCode(provideUseorgCd) {
  const url = '/auth/provideUseorgCdDupCheck'
  const params = {
    provideUseorgCd: provideUseorgCd
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function signUpCheckId(userId) {
  const url = '/auth/userIdDupCheck'
  const params = {
    userId: userId
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function checkPwd(data) {
  const url = '/auth/checkPwd'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updatePassword(data, token) {
  const url = '/auth/hfnUserPwdUpdate'
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  }
  return request.post(url, data, config)
}

export function updatePasswordAgree(data, token) {
  const url = '/api/hfnUserPwdAndTosUpdate'
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  }
  return request.post(url, data, config)
}

export function loginHis(pageUrl) {
  const url = '/auth/loginLogHis'
  const param = {
    portalType: 'user',
    pageUrl: pageUrl
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, param, config)
}
