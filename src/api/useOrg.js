import request from './request/portal'
// 수신자관리 조회 appList
export function useOrgList(data) {
    const url = '/api/useOrgList'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 수신자관리 등록 
  export function useOrgReg(data) {
    const url = '/api/useOrgReg'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 수신자관리 상태 업데이트 
  export function useOrgStatusUpd(data) {
    const url = '/api/useOrgStatusUpd'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 수신자관리 상세보기(마스킹)
  export function useOrgDetail(data) {
    const url = '/api/useOrgDetail'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 수신자관리 상세보기(마스킹X)
  export function useOrgUnDetail(data) {
    const url = '/api/useOrgUnDetail'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 수신자관리 정보 업데이트 
  export function useOrgUpd(data) {
    const url = '/api/useOrgUpd'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 수신자관리 이력 업데이트
  export function insertUseOrgHis(data) {
    const url = '/api/insertUseOrgHis'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }