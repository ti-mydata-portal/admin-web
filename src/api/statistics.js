import request from './request/portal'

// 일별API사용통계 조회
export function statsByDateApi(data) {
    const url = '/api/statsByDateApi'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 일별API사용통계 상세조회
export function statsByDateDetailApi(data) {
    const url = '/api/statsByDateDetailApi'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 일별API사용통계 조회
export function statsByDateUseorg(data) {
    const url = '/api/statsByDateUseorg'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 일별API사용통계 상세조회
export function statsByDateDetailUseorg(data) {
    const url = '/api/statsByDateDetailUseorg'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 마이데이터이용통계
  export function statsByDateMydata(data) {
    const url = '/api/statsByDateMydata'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 마이데이터이용통계
  export function statsByDateDetailMydata(data) {
    const url = '/api/statsByDateDetailMydata'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }
  // 마이데이터이용통계 엑셀
  export function statsByDateMydataExcelDown(data) {
    const url = '/api/statsByDateMydataExcelDown'
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return request.post(url, data, config)
  }