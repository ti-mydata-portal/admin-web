import request from './request/portal'

export function fetchPortalDashboard(hfnCd) {
  const url = '/api/dashBoard'
  const params = {
    hfnCd: hfnCd
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}
// grafana get
export function selectGrafana() {
  const url = '/api/selectGrafana'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, config)
}