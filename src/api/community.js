import request from './request/portal'

export function selectNoticeList(data) {
  const url = '/community/selectNoticeList'  
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function detailNotice(seqNo) {
  const url = '/community/detailNotice'
  const params = {
    seqNo: seqNo
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function searchNotice(data) {
  const url = '/community/searchNotice'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function insertNotice(data) {
  const url = '/community/insertNotice'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function deleteNotice(seqNo) {
  const url = '/community/deleteNotice'
  const params = {
    seqNo: seqNo
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function updateNotice(data) {
  const url = '/community/updateNotice'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}


export function selectQnaList(data) {
  const url = '/api/selectQnaList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function detailQna(seqNo) {
  const url = '/api/detailQna'
  const params = {
    seqNo: seqNo
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function deleteQna(data) {
  const url = '/api/deleteQna'
  const params = {
    seqList: data
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function selectFaqList(data) {
  const url = '/community/selectFaqList'  
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function detailFaq(seqNo) {
  const url = '/community/detailFaq'
  const params = {
    seqNo: seqNo
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function deleteFaq(seqNo) {
  const url = '/community/deleteFaq'
  const params = {
    seqNo: seqNo
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function insertFaq(data) {
  const url = '/community/insertFaq'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateFaq(data) {
  const url = '/community/updateFaq'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateAnswer(data) {
  const url = '/api/updateAnswer'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
