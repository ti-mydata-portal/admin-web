import request from './request/portal'

export function selApiPolicy(apiId) {
  const url = '/api/apiPolicy'
  const params = {
      apiId: apiId
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function regApiPolicy(data) {
  const url = '/api/apiPolicyRegist'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function getTokenPolicyList(data) {
  const url = '/api/getTokenPolicyList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function getTokenPolicyDetail(appKey) {
  const url = '/api/getTokenPolicyDetail'
   const param = {
        appKey: appKey
    }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, param, config)
}

export function getTokenPolicyRegistration(data) {
  const url = '/api/getTokenPolicyRegistration'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function getTokenPolicyReset(data) {
  const url = '/api/getTokenPolicyReset'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function getTokenPolicyUseSetting(data) {
  const url = '/api/getTokenPolicyUseSetting'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function checkTokenPolicyDetail(userKey) {
  const url = '/api/checkTokenPolicyDetail'
   const param = {
        userKey: userKey
    }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, param, config)
}

export function selectPrdPolicy(prdId) {
  const url = '/api/selectPrdPolicy'
  const params = {
      prdId: prdId
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function insertPrdPolicy(data) {
  const url = '/api/insertPrdPolicy'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function selectAppUsageRatio(data) {
  const url = '/api/selectAppUsageRatioPolicy'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function insertAppUsageRatio(data) {
  const url = '/api/insertAppUsageRatioPolicy'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateAppUsageRatio(data) {
  const url = '/api/updateAppUsageRatioPolicy'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function deleteAppUsageRatio(data) {
  const url = '/api/deleteAppUsageRatioPolicy'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 추가
// 유량제어 정책등록 > API 정보
export function policyGetApiList(data) {
  const url = '/api/policyGetApiList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 클라이언트ID 조회
export function policyGetClientIdList(data) {
  const url = '/api/policyGetClientIdList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 서버IP 조회
export function policyGetServerIpList(data) {
  const url = '/api/policyGetServerIpList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 정책등록
export function regPolicyService(data) {
  const url = '/api/regPolicyService'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 정책조회
export function getPolicyServiceList(data) {
  const url = '/api/getPolicyServiceList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 정책조회 상태변경
export function updPolicyServiceStatus(data) {
  const url = '/api/updPolicyServiceStatus'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 정책조회 수정조회
export function getPolicyServiceDetail(data) {
  const url = '/api/getPolicyServiceDetail'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 정책조회 수정
export function updPolicyService(data) {
  const url = '/api/updPolicyService'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 정책조회 삭제
export function delPolicyService(data) {
  const url = '/api/delPolicyService'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 레디스 정보 
export function getRedisData(data) {
  const url = '/api/getRedisData'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}