import request from './request/portal'
import { getToken } from '@/utils/auth'
import axios from 'axios'

export function selectCommCodeList(data) {
  const url = '/comm/selectCommCodeList'  
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function selectAppCodeList(data) {
  const url = '/comm/selectAppCodeList'  
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function selectPrdCodeList(data) {
  const url = '/comm/selectPrdCodeList'  
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function reqPost(url, data, poData) {
  let contentType = poData && poData.contentType;

  if(contentType == null && contentType == ""){
    contentType = 'application/json'
  }
  const config = {
    headers: {
      'Content-Type': contentType
    }
  }
  return request.post(url, data, config)
}

export function fileDl(url, data) {
  const config = {
    headers: { 
      'Content-Type': 'application/json',
      'Accept': '*/*',
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache',
    },
    responseType: 'blob'
  }
  return request.post(url, data, config)
}

export function excelDown(addUrl, param) {
  let url = location.origin;
  if(process.env.DIV == "local"){
    url = "http://localhost:9090";
  }

  let getFileNameByContentDisposition = function(contentDisposition){
    let regex = /filename[^;=\n]*=(UTF-8(['"]*))?(.*)/;
    let matches = regex.exec(contentDisposition);
    let filename;
  
    if (matches != null && matches[3]) {
      filename = matches[3].replace(/['"]/g, '');
    }
    return decodeURI(filename);
  }
  axios({
    url: url + addUrl,
    method: 'POST',
    headers: { 'Authorization': 'Bearer ' + getToken() },
    responseType: 'blob',
    params: param
  }).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]))
    const link = document.createElement('a')
    let fileName = response.headers['content-disposition']
    fileName = getFileNameByContentDisposition(fileName)
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
  })
}