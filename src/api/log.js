import request from './request/portal'

export function fetchPortalLogList(data) {
  const url = '/api/portalLogs'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchPortalLogDetail(trxId) {
  const url = '/api/portalLog'
  const data = {
    trxId: trxId
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiLogList(data) {
  const url = '/api/apiLogs'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApiLogDetail(data) {
  const url = '/api/apiLog'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
