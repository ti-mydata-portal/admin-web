import request from './request/portal'

export function fetchOfficeListAll() {
  const url = '/api/useorgsAll'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, config)
}

export function fetchOfficeList(data) {
  const url = '/api/useorgs'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchUser(data) {
  const url = '/api/user'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchUserList(data) {
  const url = '/api/users'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateUserStatus(userKey) {
  const url = '/api/userStatCdChange'
  const params = {
    userKey: userKey
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function checkId(userId) {
  const url = '/api/userIdDupCheck'
  const params = {
    userId: userId
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function addUser(data) {
  const url = '/api/userRegist'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchHfnUserById(userKey) {
  const url = '/api/hfnUserById'
  const params = {
    userKey: userKey
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}
export function getMyInfo (userKey) {
  const url = '/api/hfnUser'
  const params = {
    userKey: userKey
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function editUser(data) {
  const url = '/api/userUpdate'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function getTemporaryPassword(id) {
  const url = '/api/tmpPwdIssue'
  const params = {
    userKey: id
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function getUserWidhdraw(data) {
  const url = '/api/getUserWithdraw'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// ================================================ //
// ===================== Hfn User  ================== //
// ================================================ //

export function fetchHfnUserList(data) {
  const url = '/api/hfnUsers'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function addHfnUser(data) {
  const url = '/api/hfnUserRegist'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updAdminUserStatus(userKey) {
  const url = '/api/HfnUserStatCdChange'
  const params = {
    userKey: userKey
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function checkHfnId(params) {
  const url = '/api/hfnIdDupCheck'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}

export function updateHfnUser(data) {
  const url = '/api/hfnUserUpdate'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function myInfoUpdate(data) {
  const url = '/api/hfnMyInfoUpdate'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

// ================================================ //
// ======================  Approve  ================== //
// ================================================ //

export function getAplvHisList(data) {
  const url = '/api/getAplvHisList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchApproveList(data) {
  const url = '/api/aplvs'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function approveApply(data) {
  const url = '/admin/approval/prodAplvApproval'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function approveReject(data) {
  const url = '/admin/approval/prodAplvReject'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function searchMember(data) {
  const url = '/api/findHfnUser'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchAplvLineReg(data) {
  const url = '/api/aplvLineSetting'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function hfnLoginLockRelease(data) {
  const url = '/api/hfnLoginLockReleasse'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function userLoginLockRelease(data) {
  const url = '/api/userLoginLockRelease'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

// 개인 정보 해제 로그 등록
export function registerPrivacyReleaseLog(data) {
  const url = '/api/registerPrivacyReleaseLog'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function selectCertMailList(data) {
  const url = '/api/selectCertMailList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function detailCertMail(seq) {
  const url = '/api/detailCertMail'
  const data = {
    seq: seq
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function fetchBatchIdList(data) {
  const url = '/api/fetchBatchIdList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function batchLogList(data) {
  const url = '/api/batchLogList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function selectTrx(data) {
  const url = '/api/selectTrx'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function regTrx(data) {
  const url = '/api/regTrx'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function detailTrx(data) {
  const url = '/api/detailTrx'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function updateTrx(data) {
  const url = '/api/updateTrx'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function deleteTrx(data) {
  const url = '/api/deleteTrx'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// ===================================== //
// =============== NICE  =============== //
// ===================================== //

export function prdAplvList(data) {
  const url = '/admin/approval/prdAplvList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function prdAplvDetail(aplvSeqNo, prdCtgy) {
  const url = '/admin/approval/prdAplvDetail'
  const params = {
    aplvSeqNo: aplvSeqNo,
    prdCtgy: prdCtgy
  }
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, params, config)
}
// TO-BE
// 계정등록

export function addAccount(data) {
  const url = '/api/addAccount'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 계정관리 상태값 변경
export function accountStatusUpd(data) {
  const url = '/api/accountStatusUpd'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 마스킹해제 
export function accountUnMasking(data) {
  const url = '/api/accountUnMasking'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 계정관리 수정
export function accountUpd(data) {
  const url = '/api/accountUpd'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}
// 잠금해제
export function accountLockRelease(data) {
  const url = '/api/accountLockRelease'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}