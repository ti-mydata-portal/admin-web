import axios from 'axios'
import { Message, MessageBox } from 'element-ui'
import store from '@/store'
import i18n from '@/lang'
import { getToken, setToken } from '@/utils/auth'
import router from '@/router'

// create an axios instance
const service = axios.create({
  baseURL: process.env.BASE_API,
  timeout: 100000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    if (store.getters.token) {
      config.headers['Authorization'] = 'Bearer ' + getToken()
    }

    if (config.url.indexOf('logout') >= 0) {
      config.headers['logout'] = 'Y'
    }

    if (config.url.indexOf('tokenTimeContinue') >= 0) {
      config.headers['tokenTimeContinue'] = 'Y'
    }

    return config
  },
  error => {
    // Do something with request error
    Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {

    if (response.headers.accesstoken !== undefined && response.headers.accesstoken !== null && response.headers.accesstoken !== "") {
      if (response.headers.accesstoken === 'expired') {
        MessageBox.alert('로그인 유효 시간이 만료되어 자동 로그아웃 되었습니다.', i18n.t('title.logout'), {
          confirmButtonText: i18n.t('btn.confirm'),
          type: 'warning',
          callback: action => {
            store.dispatch('LogOut').then(() => {
              location.reload()
            })
          }
        })
      } else {
        setToken(response.headers.accesstoken)
        // 토큰 갱신
        store.dispatch('SetToken').then(res => {

        }).catch((err) => {
          alert('인증 정보가 유효하지 않습니다.')
          this.$store.dispatch('LogOut').then(() => {
            location.reload()
          })
        })
      }
    }

    if( response.data.errorCode !== undefined ) {
      alert(response.data.message)
      return Promise.reject(response.data.message)
    }

    return response.data
  },
  error => {
    // response 상태가 정상 아니면
    // response 리턴 해주는 메시지 알림처리
    const errRes = error.response.data
    const errorMsg = errRes.message
    const errorcode = error.response.headers.errorcode

    if (errorMsg.indexOf('3개월') >= 0) {
      return errorMsg
    }

    if (errRes.status === 401 && errorcode != null) {
      MessageBox.alert(errorMsg, i18n.t('title.info'), {
        confirmButtonText: i18n.t('btn.confirm'),
        type: 'warning',
        callback: action => {
          // store.dispatch('FedLogOut').then(() => {
          store.dispatch('LogOut').then(() => {
            location.reload()
          })
        }
      })
    } else {
      if (errorMsg !== null && errorMsg !== undefined && errorMsg !== '') {
        Message({
          message: errorMsg,
          type: 'error',
          duration: 3 * 1000
        })

        return Promise.reject('error')
      }
    }
  }
)

export default service
