import request from './request/portal'
import longRequest from './request/longRequest'

export function getApiList(data) {
  const url = '/api/devGuides'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}

export function procGateway(data) {
  //hana
  //const url = '/api/guideGw'
  
  //nice
  const url = '/api/devGuideGw'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return longRequest.post(url, data, config)
}

export function devApiList(data) {
  const url = '/api/devApiList'
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return request.post(url, data, config)
}